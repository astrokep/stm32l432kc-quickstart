// #![deny(unsafe_code)]
// #![deny(warnings)]
#![no_std]
#![no_main]

extern crate cortex_m;

#[macro_use]
extern crate cortex_m_rt as rt;
extern crate cortex_m_semihosting as sh;
extern crate panic_semihosting;
extern crate stm32l4xx_hal as hal;

use crate::hal::{prelude::*, delay::Delay};
use crate::rt::{entry, ExceptionFrame};

#[entry]
fn main() -> ! {
    if let (Some(cp), Some(dp)) = (
        cortex_m::Peripherals::take(),
        hal::stm32::Peripherals::take(),
    ) {
        
        loop {
            // Put code here
        }
    }
    loop{}
}

#[exception]
fn HardFault(ef: &ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}